#include <stdio.h>
#include <stdlib.h>

#include "../struct.h"

#include "./update_height.h"

struct avl_node* private_avl_rotate_left_node(
	struct avl_node* x,
	struct avl_node* parent)
{
	struct avl_node* y = x->right;
	struct avl_node* t = y->left;
	y->left = x;
	x->right = t;
	y->parent = parent;
	x->parent = y;
	if(t)
	{
		t->parent = x;
	}
	private_avl_update_height(x);
	private_avl_update_height(y);
	return y;
}

