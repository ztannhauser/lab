#include <stdio.h>
#include <stdlib.h>

#include "../struct.h"

#include "./update_height.h"

struct avl_node* private_avl_rotate_right_node(
	struct avl_node* y,
	struct avl_node* parent)
{
	struct avl_node* x = y->left;
	struct avl_node* t = x->right;
	x->right = y;
	y->left = t;
	x->parent = parent;
	y->parent = x;
	if(t)
	{
		t->parent = y;
	}
	private_avl_update_height(y);
	private_avl_update_height(x);
	return x;
}

