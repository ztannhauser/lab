
struct avl_node* avl_tree_search(
	struct avl_tree* this,
	int (*compare)(const void*, const void*),
	const void* b);
