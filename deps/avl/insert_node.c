#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "./struct.h"

#include "./private/consider_rebalance.h"

struct avl_tree* avl_insert_node(
	struct avl_tree* this,
	struct avl_node* new)
{
	struct avl_node* go(
		struct avl_node* node,
		struct avl_node* node_parent,
		struct avl_node* prev,
		struct avl_node* next
	)
	{
		if(node)
		{
			int c = this->compare(node->data, new->data);
			if(c > 0)
			{
				node->left = go(node->left, node, prev, node);
			}
			else
			{
				node->right = go(node->right, node, node, next);
			}
			node = private_avl_consider_rebalance(
				node, node_parent);
		}
		else
		{
			new->height = 0;
			new->parent = node_parent;
			if(prev)
			{
				new->prev = prev;
				prev->next = new;
			}
			else
			{
				this->leftmost = new;
			}
			if(next)
			{
				new->next = next;
				next->prev = new;
			}
			else
			{
				this->rightmost = new;
			}
			node = new;
		}
		return node;
	}
	this->root = go(this->root, 0, 0, 0);
	this->n++;
	return this;
}

