
#include <X11/Xlib.h>

#include <debug.h>

#include "defines.h"
#include "struct.h"

#if 0
font = XLoadFont(menu->display, sfont);
XSetFont(display, this->super.gc, menu->font);
#endif

void submenu_entry_resize_to_fit_text(struct submenu_entry* this)
{
	Display* display;
	Window window;
	GC gc;
	int w, h;
	int direction, ascent, descent;
	XCharStruct xchar;
	ENTER;
	
	display = this->super.display;
	window = this->super.super.window;
	gc = this->super.gc;
	
	XQueryTextExtents(display, XGContextFromGC(gc),
		this->text, this->strlen_text,
		&direction, &ascent, &descent,
		&xchar);
		
	w = MARGIN + xchar.width + MARGIN;
	h = MARGIN + ascent + descent + MARGIN;
	
	this->string_bottom_left.x = MARGIN;
	this->string_bottom_left.y = MARGIN + ascent;
	
	this->super.minumum.width = w;
	this->super.minumum.height = h;
	
	XResizeWindow(display, window, w, h);
	
	EXIT;
}


















