
#include <debug.h>

#include "../../container/defines.h"
#include "../../container/struct.h"
#include "../../container/map.h"

#include "struct.h"
#include "on_expose.h"

void submenu_entry_on_gain_hovering(
	struct submenu_entry* this,
	XMotionEvent* event)
{
	Window parent_window;
	Atom sendtos_atom;
	int x, y;
	ENTER;

	this->is_mouse_inside = true;
	submenu_entry_on_expose(this, NULL);
	
	parent_window = this->super.parent_container->super.window;
	sendtos_atom = this->super.parent_container->sendtos_atom;
	
	x = this->super.parent_container->super.x;
	y = this->super.parent_container->super.y;
	
	x += this->super.parent_container->super.width - MENU_CONTAINER_MARGIN;
	
	x += this->super.super.x;
	y += this->super.super.y;
	
	
	menu_container_map(
		this->subcontainer,
		x, y,
		parent_window,
		sendtos_atom);
	
	EXIT;
}
	
