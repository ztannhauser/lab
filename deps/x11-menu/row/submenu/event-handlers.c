
#include <x11-loop/events.h>

#include "on_expose.h"
#include "on_mouse_enter.h"
#include "on_mouse_leave.h"

struct events submenu_entry_events = 
{
	INIT_ALL_FIELDS_NULL,
	.on_expose = submenu_entry_on_expose,
	.on_mouse_enter = submenu_entry_on_mouse_enter,
	.on_mouse_leave = submenu_entry_on_mouse_leave,
};
