
#include <debug.h>

#include "../../container/struct.h"

#include "struct.h"
#include "on_expose.h"

// unmap subcontainer,
// redraw expse

void submenu_entry_on_lost_hovering(struct submenu_entry* this)
{
	Display* display;
	Window subcontainer_window;
	ENTER;
	
	display = this->super.display;
	subcontainer_window = this->subcontainer->super.window;
	
	this->is_mouse_inside = false;
	submenu_entry_on_expose(this, NULL);
	
	XUnmapWindow(display, subcontainer_window);
	
	EXIT;
}
