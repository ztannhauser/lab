

// I'm just one row
// but on the event that the mouse enters my row,
// I'm going to map my submenu window

// my submenu window contains a row full of subwindows

#include <stdbool.h>

#include "../struct.h"

struct submenu_entry
{
	struct menu_row super;

	const char* text;
	int strlen_text;

	struct {int x, y;} string_bottom_left;
	
	bool is_mouse_inside;

	struct menu_container* subcontainer;
};
