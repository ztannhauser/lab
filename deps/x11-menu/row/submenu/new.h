
struct submenu_entry* new_submenu_entry(
	Display* display,
	struct x11_loop* loop,
	const char* text,
	struct menu_container* subcontainer);
