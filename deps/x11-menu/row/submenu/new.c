
#include <X11/Xlib.h>

#include <debug.h>

#include "../../container/struct.h"

#include "../new.h"

#include "struct.h"
#include "event-handlers.h"
#include "callbacks.h"
#include "change_text.h"

#define SUBMENU_ENTRY_EVENT_MASK ( \
	ExposureMask | \
	EnterWindowMask | \
	LeaveWindowMask | \
	StructureNotifyMask | \
	0)

struct submenu_entry* new_submenu_entry(
	Display* display,
	struct x11_loop* loop,
	const char* text,
	struct menu_container* subcontainer)
{
	struct submenu_entry* this;
	int w, h;
	Font font;
	ENTER;

	this = new_menu_row(
		display,
		loop,
		SUBMENU_ENTRY_EVENT_MASK,
		&(submenu_entry_events),
		&(submenu_entry_callbacks),
		sizeof(struct submenu_entry));
	
	this->text = text;
	this->is_mouse_inside = false;

	this->subcontainer = subcontainer;
	subcontainer->submenu = this;
	
	submenu_entry_change_text(this, text);
	
	EXIT;
	return this;
}







