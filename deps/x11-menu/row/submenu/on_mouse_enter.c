
#include <X11/Xlib.h>

#include <debug.h>

#include "../../container/switch_hovering.h"

#include "struct.h"
#include "on_expose.h"

void submenu_entry_on_mouse_enter(
	struct submenu_entry* this,
	XMotionEvent* event)
{
	ENTER;

	menu_container_switch_hovering(
		this->super.parent_container,
		this,
		event);
	
	EXIT;
}
