
#include <X11/Xlib.h>

#include <x11-loop/widget/struct.h>

struct menu_row
{
	struct widget super;
	void* loop_token;
	
	Display* display;
	struct menu_container* parent_container;
	struct row_callbacks* callbacks;
	GC gc;
	
	struct {int width, height;} minumum;
};
