
#include <X11/Xlib.h>

struct menu_row;

struct row_callbacks
{
	void (*on_gain_hovering)(struct menu_row* this, XMotionEvent* event);
	void (*on_lost_hovering)(struct menu_row* this);
	void (*delete)(struct menu_row* this);
};

