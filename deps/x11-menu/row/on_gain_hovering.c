
#include <X11/Xlib.h>

#include <debug.h>

#include "callbacks.h"
#include "struct.h"

void menu_row_on_gain_hovering(
	struct menu_row* this,
	XMotionEvent* event)
{
	ENTER;
	
	this->callbacks->on_gain_hovering(this, event);
	
	EXIT;
}
