
#include <X11/Xlib.h>

#include <debug.h>

#include <x11-loop/events.h>

#include "../new.h"

#include "struct.h"
#include "callbacks.h"
#include "event-handlers.h"
#include "change_text.h"

#define MENU_ENTRY_EVENT_MASK ( \
	ExposureMask | \
	EnterWindowMask | \
	LeaveWindowMask | \
	ButtonPressMask | \
	ButtonReleaseMask | \
	StructureNotifyMask | \
	0)

struct menu_entry* new_menu_entry(
	Display* display,
	struct x11_loop* loop,
	const char* text,
	void* value)
{
	struct menu_entry* this;
	int w, h;
	Font font;
	ENTER;

	
	verpv(menu_entry_events.on_expose);
	
	this = new_menu_row(
		display,
		loop,
		MENU_ENTRY_EVENT_MASK,
		&(menu_entry_events),
 		&(menu_entry_callbacks),
		sizeof(struct menu_entry));
	
	this->is_mouse_inside = false;
	
	this->value = value;
	
	menu_entry_change_text(this, text);
	
	EXIT;
	return this;
}







