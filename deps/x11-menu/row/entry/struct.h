
#include <stdbool.h>

#include "../struct.h"

struct menu_entry
{
	struct menu_row super;

	const char* text;
	int strlen_text;
	
	struct {int x, y;} string_bottom_left;
	
	bool is_mouse_inside;

	void* value;
};
