
#include <debug.h>

#include "struct.h"
#include "on_expose.h"

#if 0
	this->is_mouse_inside = false;
	menu_entry_on_expose(this, NULL);
#endif

void menu_entry_on_lost_hovering(struct menu_entry* this)
{
	ENTER;
	
	this->is_mouse_inside = false;
	menu_entry_on_expose(this, NULL);
	
	EXIT;
};
