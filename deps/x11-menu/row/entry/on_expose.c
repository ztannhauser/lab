
#include <X11/Xlib.h>
#include <stdbool.h>

#include <debug.h>

#include "struct.h"

void menu_entry_on_expose(struct menu_entry* this, XExposeEvent* event)
{
	Display* display;
	Window window;
	GC gc;
	int width, height;
	ENTER;
	
	display = this->super.display;
	window = this->super.super.window;
	gc = this->super.gc;
	width = this->super.super.width;
	height = this->super.super.height;
	
	XSetForeground(display, gc, 
		this->is_mouse_inside ? 0xFF00 : 0xFFFF);
	
	XFillRectangle(display, window, gc,
		0, 0, width, height);
	
	XSetForeground(display, gc, 
		this->is_mouse_inside ? -1 : 0);
	
	XDrawString(display, window, gc,
		this->string_bottom_left.x,
		this->string_bottom_left.y,
		this->text,
		this->strlen_text);
	
	EXIT;
}
