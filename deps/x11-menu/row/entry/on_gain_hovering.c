
#include <debug.h>

#include "struct.h"
#include "on_expose.h"

#if 0
	this->is_mouse_inside = true;
	menu_entry_on_expose(this, NULL);
#endif

void menu_entry_on_gain_hovering(
	struct menu_entry* this,
	XMotionEvent* event)
{
	ENTER;
	
	this->is_mouse_inside = true;
	menu_entry_on_expose(this, NULL);
	
	EXIT;
};
