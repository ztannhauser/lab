
#include <x11-loop/events.h>

#include "on_expose.h"
#include "on_mouse_enter.h"
#include "on_mouse_leave.h"
#include "on_button_released.h"

struct events menu_entry_events = 
{
	INIT_ALL_FIELDS_NULL,
	.on_expose = menu_entry_on_expose,
	.on_mouse_enter = menu_entry_on_mouse_enter,
	.on_mouse_leave = menu_entry_on_mouse_leave,
	.on_button_released = menu_entry_on_button_released,
};
