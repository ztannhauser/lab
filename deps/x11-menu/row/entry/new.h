
struct menu_entry* new_menu_entry(
	Display* display,
	struct x11_loop* loop,
	const char* text,
	void* value);
