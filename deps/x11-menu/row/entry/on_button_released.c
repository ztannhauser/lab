
#include <X11/Xlib.h>
#include <string.h>

#include <debug.h>

#include "../../container/struct.h"
#include "struct.h"

// http://www.smegware.com/engineering/software/library/library_menu.php

void menu_entry_on_button_released(struct menu_entry* this, XButtonEvent* unused)
{
	Display* display;
	Window send_to;
	XEvent event;
	Atom sendtos_atom;
	ENTER;

	display = this->super.display;
	send_to = this->super.parent_container->super.window;
	sendtos_atom = this->super.parent_container->sendtos_atom;
	
	verpv(sendtos_atom);

	memset(&event, 0, sizeof(event));
	
	event.xclient.type         = ClientMessage;
	event.xclient.display      = display;
	event.xclient.window       = send_to;
	event.xclient.message_type = sendtos_atom;
	event.xclient.format       = 8;
	
	memcpy(event.xclient.data.b, &(this->value), sizeof(void*));
	
	verpv(event.xclient.message_type);
	
	XSendEvent(display, send_to, 0, 0, &event);
	
	EXIT;
}





























