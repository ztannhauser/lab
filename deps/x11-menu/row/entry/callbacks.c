
#include <stdio.h>

#include "../callbacks.h"

#include "on_gain_hovering.h"
#include "on_lost_hovering.h"
#include "delete.h"

struct row_callbacks menu_entry_callbacks = 
{
	.on_gain_hovering = menu_entry_on_gain_hovering,
	.on_lost_hovering = menu_entry_on_lost_hovering,
	.delete = delete_menu_entry,
	NULL
};
