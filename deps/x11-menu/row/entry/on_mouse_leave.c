
#include <X11/Xlib.h>

#include <debug.h>

#include "../../container/switch_hovering.h"

#include "struct.h"
#include "on_expose.h"

void menu_entry_on_mouse_leave(struct menu_entry* this, XLeaveWindowEvent* event)
{
	ENTER;
	
	menu_container_switch_hovering(
		this->super.parent_container,
		NULL,
		NULL);
	
	EXIT;
}
