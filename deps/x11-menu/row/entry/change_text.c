
#include <string.h>

#include <debug.h>

#include "struct.h"
#include "resize_to_fit_text.h"

void menu_entry_change_text(struct menu_entry* this, const char* text)
{
	ENTER;
	
	this->text = strdup(text);
	this->strlen_text = strlen(text);
	
	menu_entry_resize_to_fit_text(this);
	
	EXIT;
}
