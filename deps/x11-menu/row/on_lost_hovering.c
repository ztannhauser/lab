
#include <debug.h>

#include "callbacks.h"
#include "struct.h"

void menu_row_on_lost_hovering(struct menu_row* this)
{
	ENTER;
	
	this->callbacks->on_lost_hovering(this);
	
	EXIT;
}
