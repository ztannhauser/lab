
#include <X11/Xlib.h>
#include <stdlib.h>

#include <debug.h>

#include <x11-loop/events.h>
#include <x11-loop/push.h>
#include <x11-loop/widget/new.h>

#include "struct.h"

struct menu_row* new_menu_row(
	Display* display,
	struct x11_loop* loop,
	int event_mask,
	struct events* events,
	struct row_callbacks* callbacks,
	int size)
{
	struct menu_row* this;
	Window window;
	ENTER;
	
	verpv(events->on_expose);
	
	// setup widget:
	{
		this = new_widget(
			events,
			window = XCreateSimpleWindow(
				display,
				DefaultRootWindow(display), // will be reparted to container
				0, 0, // will later be repositioned
				100, 100, // will later resized by child,
				0, 0,
				0x334455
			),
			0, 0,
			100, 100,
			size);
	}
	
	// resigster and set event mask:
	{
		this->loop_token = x11_loop_push(loop, this);
		XSelectInput(display, window, event_mask);
	}
	
	// create GC:
	{
		this->gc = XCreateGC(display, window, 0, NULL);
	}
	
	this->parent_container = NULL; // set my parent container
	
	this->display = display;
	this->callbacks = callbacks;
	
	EXIT;
	return this;
}
















