
#include <x11-loop/struct.h>

struct menu_row* new_menu_row(
	Display* display,
	struct x11_loop* loop,
	int event_mask,
	struct events* events,
	struct row_callbacks* callbacks,
	int size);
