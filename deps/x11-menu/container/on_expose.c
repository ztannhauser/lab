
#include <X11/Xlib.h>

#include <debug.h>

#include "struct.h"

void menu_container_on_expose(struct menu_container* this, XExposeEvent* event)
{
	ENTER;
	
	XSetForeground(this->display, this->gc, 0x556677);
	
	XFillRectangle(this->display, this->super.window, this->gc, 
		event->x, event->y, event->width, event->height);
	
	EXIT;
}
