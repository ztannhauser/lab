
#include <X11/Xlib.h>

#include <debug.h>

#include <x11-loop/grabbed-mode/leave.h>
#include <x11-loop/grabbed-mode/pop.h>

#include "../row/on_lost_hovering.h"
#include "struct.h"

void menu_container_on_unmap(
	struct menu_container* this,
	XMapEvent* event)
{
	ENTER;
	
	// and remove me from grabbed list:
	x11_loop_grabbed_mode_pop(this->loop, this->grabbed_loop_token);
	
	if(this->hovering_row != NULL)
	{
		menu_row_on_lost_hovering(this->hovering_row);
	}
	
	if(this->submenu == NULL) // if I'm root
	{
		x11_loop_leave_grabbed_mode(this->loop);
	}
	
	EXIT;
}
