


// I'm the whole container, who shows the rows

// multiple of me can be open at once,

// we need to communicate about:
	// the user clicked nowhere: everyone should close.
	// the mouse has just left one of the submenu rows: all containers downstream
		// should close

#include <X11/Xlib.h>

#include <array/struct.h>

#include <x11-loop/widget/struct.h>

struct menu_container
{
	struct widget super;
	struct x11_loop* loop;
	Display* display;
	void* normal_loop_token;
	void* grabbed_loop_token;
	GC gc;

	struct array rows; // elements of type 'struct menu_row*'
	
	Window sendto; // destination to send data back to
	Atom sendtos_atom; // atom to use when calling back to caller
	
	struct submenu_entry* submenu; // if null, them I'm the root
	
	struct menu_row* hovering_row;
	
};






























