
#include <x11-loop/events.h>

#include "on_expose.h"
#include "on_root_button_pressed.h"
#include "on_root_button_released.h"
#include "on_root_mouse_motion.h"

struct events menu_container_root_events = 
{
	INIT_ALL_FIELDS_NULL,
	.on_button_pressed = menu_container_on_root_button_pressed,
	.on_button_released = menu_container_on_root_button_released,
	.on_mouse_motion = menu_container_on_root_mouse_motion,
};
