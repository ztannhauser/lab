
#include <stdlib.h>
#include <X11/Xlib.h>
#include <stdbool.h>
#include <stdarg.h>

#include <debug.h>

#include <array/new.h>
#include <array/push_n.h>

#include <x11-loop/push.h>
#include <x11-loop/widget/new.h>

#include "../row/struct.h"

#include "struct.h"
#include "event-handlers.h"
#include "repos_children_and_resize.h"

#define LAST_ARG_BEFORE_ROWS loop

struct menu_container* new_menu_container(
	Display* display,
	struct x11_loop* loop,
	... // rows
)
{
	struct menu_container* this;
	Window window;
	va_list ap;
	struct menu_row* child;
	ENTER;
	
	
	verpv(menu_container_events.on_expose);
	
	// Setup widget:
	{
		XSetWindowAttributes attrib;

		attrib.save_under = true;
		attrib.override_redirect = true;
		attrib.background_pixel = 0;
		
		this = new_widget(
			&(menu_container_events),
			window = XCreateWindow(
				display, DefaultRootWindow(display),
				0, 0,
				200, 300, // will be resized later
				0, // border width
				CopyFromParent, CopyFromParent, CopyFromParent,
				0
				| CWBackPixel
				| CWSaveUnder
				| CWOverrideRedirect
				| 0,
				&attrib
			),
			0, 0,
			200, 300,
			sizeof(struct menu_container)
		);
		
		verpv(window);
		
	}
	
	// register and set event mask:
	{
		this->normal_loop_token = x11_loop_push(loop, this);
		XSelectInput(display, window, MENU_CONTAINER_EVENT_MASK);
	}
	
	// save other X11 stuff:
	{
		this->loop = loop;
		this->display = display;
	}
	
	// create GC:
	{
		this->gc = XCreateGC(display, window, 0, NULL);
	}
	
	this->submenu = NULL;
	
	// read in children, map them, and reparent
	{
		this->rows = new_array(struct menu_row*);
		
		va_start(ap, LAST_ARG_BEFORE_ROWS);
		
		while(child = va_arg(ap, struct menu_row*))
		{
			XReparentWindow(display, child->super.window, window, 0, 0);
			
			XMapWindow(display, child->super.window);
		}
		
		va_end(ap);
	}
	
	
	EXIT;
	return this;
}
















