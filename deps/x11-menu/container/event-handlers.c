
#include <x11-loop/events.h>

#include "on_expose.h"
#include "on_map.h"
#include "on_client_message.h"
#include "on_unmap.h"
#include "on_gained_child_notify.h"
#include "on_child_configure_notify.h"
#include "on_lost_child_notify.h"

struct events menu_container_events = 
{
	INIT_ALL_FIELDS_NULL,
	.on_expose = menu_container_on_expose,
	.on_map = menu_container_on_map,
	.on_unmap = menu_container_on_unmap,
	.on_client_message = menu_container_on_client_message,
	.on_gained_child_notify = menu_container_on_gained_child_notify,
	.on_lost_child_notify = menu_container_on_lost_child_notify,
	.on_child_configure_notify = menu_container_on_child_configure_notify
};
