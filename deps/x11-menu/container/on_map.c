
#include <X11/Xlib.h>

#include <debug.h>

#include <x11-loop/grabbed-mode/enter.h>
#include <x11-loop/grabbed-mode/push.h>

#include "struct.h"

#include "root-event-handlers.h"

void menu_container_on_map(struct menu_container* this, XMapEvent* event)
{
	ENTER;
	
	if(this->submenu == NULL) // if I'm root:
	{
		x11_loop_enter_grabbed_mode(
			this->loop,
			this,
			MENU_CONTAINER_ROOT_EVENT_MASK,
			&menu_container_root_events
		);
	}

	this->grabbed_loop_token =
		x11_loop_grabbed_mode_push(this->loop, this);
	

	EXIT;
}
	
