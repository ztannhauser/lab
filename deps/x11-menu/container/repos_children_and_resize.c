
#include <X11/Xlib.h>

#include <debug.h>

#include <array/struct.h>

#include "../row/struct.h"
#include "struct.h"
#include "defines.h"

void menu_container_repos_children_and_resize(struct menu_container* this)
{
	Display* display;
	Window window;
	int w, h, i, n;
	struct menu_row* ele;
	ENTER;
	
	display = this->display;
	window = this->super.window;
	
	// read in children, map them, give them a location, and reparent
	// also determine correct size for container:
	{
		w = 0, h = 0;
		
		h += MENU_CONTAINER_MARGIN;
		
		for(i = 0, n = this->rows.n;i < n;i++)
		{
			ele = array_index(this->rows, i, struct menu_row*);
			
			verpv(ele->super.width);
			verpv(ele->super.height);
			
			if(ele->minumum.width > w) w = ele->minumum.width;
			
			if(ele->super.x != MENU_CONTAINER_MARGIN || ele->super.y != h)
			{
				XMoveWindow(display, ele->super.window, MENU_CONTAINER_MARGIN, h);
			}
			
			h += ele->minumum.height;
			
			h += MENU_CONTAINER_MARGIN;
			
		}
		
		verpv(w); verpv(h);
	}

	// reize small children to fit container's width:
	{
		for(i = 0;i < n;i++)
		{
			ele = array_index(this->rows, i, struct menu_row*);
			if(ele->super.width != w ||
				ele->super.height != ele->minumum.height)
			{
				XResizeWindow(display, ele->super.window, w,
					ele->minumum.height);
			}
		}
	}
	
	w += MENU_CONTAINER_MARGIN * 2;
	
	// resize container to fit:
	{
		if(this->super.width != w || this->super.height != h)
		{
			XResizeWindow(display, window, w, h);
		}
	}
		
	EXIT;
}














