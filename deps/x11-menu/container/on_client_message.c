
#include <X11/Xlib.h>

#include <debug.h>

#include "struct.h"

void menu_container_on_client_message(
	struct menu_container* this,
	XClientMessageEvent* event)
{
	Window sendto;
	ENTER;
	
	if(event->message_type == this->sendtos_atom)
	{
		// send this event higher up:
		{
			sendto = this->sendto;
			verpv(sendto);
			
			event->window = sendto;
			
			XSendEvent(this->display, sendto, 0, 0, event);
		}
		
		// unmap myself:
		{
			verpv(this->super.window);
			XUnmapWindow(this->display, this->super.window);
		}
		
	}
	else
	{
		TODO;
	}
	
	EXIT;
}
