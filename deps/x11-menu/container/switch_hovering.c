
#include <X11/Xlib.h>

#include <debug.h>

#include "../row/on_gain_hovering.h"
#include "../row/on_lost_hovering.h"

#include "struct.h"

// if(this->hovering_row != null)
	// menu_row_on_lost_hovering(this->hovering_row);
	// this->hovering_row = null

void menu_container_switch_hovering(
	struct menu_container* this,
	struct menu_row* row,
	XMotionEvent* event)
{
	ENTER;
	
	verpv(this);
	verpv(this->hovering_row);
	verpv(row);
	
	if(this->hovering_row != row)
	{
		if(this->hovering_row != NULL)
		{
			menu_row_on_lost_hovering(this->hovering_row);
		}
		
		if(row)
		{
			menu_row_on_gain_hovering(row, event);
		}
		
		this->hovering_row = row;
	}
	EXIT;
}
