
#include <X11/Xlib.h>

#include <debug.h>

#include <x11-loop/grabbed-mode/leave.h>

#include "struct.h"

void menu_container_on_root_button_released(
	struct menu_container* this,
	XButtonEvent* event)
{
	ENTER;
	
	// Nothing was clicked on!
	
	XUnmapWindow(this->display, this->super.window);
	
	EXIT;
}
