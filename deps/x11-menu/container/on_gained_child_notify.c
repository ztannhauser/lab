
#include <X11/Xlib.h>

#include <debug.h>

#include <array/push_n.h>

#include "../row/struct.h"
#include "struct.h"
#include "repos_children_and_resize.h"

void menu_container_on_gained_child_notify(
	struct menu_container* this,
	struct menu_row* new_child,
	XReparentEvent* event)
{
	ENTER;

	verpv(new_child);
	
	new_child->parent_container = this;
	
	array_push_n(&(this->rows), &new_child);
	
	menu_container_repos_children_and_resize(this);
	
	EXIT;
}
