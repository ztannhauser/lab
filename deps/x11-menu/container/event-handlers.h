
#include <x11-loop/events.h>

#define MENU_CONTAINER_EVENT_MASK (\
	ExposureMask | \
	StructureNotifyMask | \
	SubstructureNotifyMask | \
	0)


struct events menu_container_events;
