
#include <X11/Xlib.h>

#include <debug.h>

#include <x11-loop/widget/struct.h>

#include "repos_children_and_resize.h"

void menu_container_on_child_configure_notify(
	struct menu_container* this,
	struct widget* child,
	XConfigureEvent* event)
{
	ENTER;
	
	menu_container_repos_children_and_resize(this);
	
	EXIT;
}
