
#include <X11/Xlib.h>

#include <debug.h>

#include <x11-loop/grabbed-mode/enter.h>
#include <x11-loop/grabbed-mode/push.h>

#include "struct.h"

static void map(
	struct menu_container* this,
	int x, int y)
{
	Display* display;
	Window window;
	
	ENTER;
	
	display = this->display;
	window = this->super.window;

	XMoveWindow(display, window, x, y);
	XMapWindow(display, window);
	XRaiseWindow(display, window);
	
	this->hovering_row = NULL;
	
	EXIT;
}

void menu_container_map(
	struct menu_container* this,
	int x, int y,
	Window sendto,
	Atom sendtos_atom)
{
	ENTER;

	this->sendto = sendto;
	this->sendtos_atom = sendtos_atom;
	
	map(this, x, y);
	
	EXIT;
}









