
#include <X11/Xlib.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#include <debug.h>

#include <array/new.h>
#include <avl/init_tree.h>

#include "struct.h"

#include "widget/compare.h"

struct x11_loop* new_x11_loop(Display* display)
{
	struct x11_loop* this;
	ENTER;
	this = malloc(sizeof(struct x11_loop));

	this->should_continue = true;
	
	this->display = display;
	this->root_window = DefaultRootWindow(display);
	verpv(this->root_window);
	
	this->mode = m_normal;
	
	avl_init_tree(&(this->widgets),
		private_x11_loop_compare_widgets, NULL);
		
	this->timeout_functions = new_array(void*);
	
	EXIT;
	return this;
}























