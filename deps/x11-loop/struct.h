
#include <X11/Xlib.h>
#include <stdbool.h>

#include <avl/struct.h>

#include <array/struct.h>

struct x11_loop
{
	Display* display;
	Window root_window;
	
	bool should_continue;

	struct avl_tree widgets; // elements of type 'struct widget*'
	
	struct array timeout_functions; // elements of type 'void (*)(double dt)'

	enum {m_normal, m_grabbed} mode;
	
	struct // fields for specific modes:
	{
		struct // normal mode
		{
			
		} normal_mode_fields;
		
		struct // grabbed pointer mode
		{
			struct {int x, y;} offset;
			struct events* root_events;
			struct widget* root_widget;
			struct widget* hovering_widget;
			struct array grabbed_widgets;
		}
		grabbed_mode;
	};
};
