
#include <debug.h>

#include <avl/search.h>

#include <array/push_n.h>

#include "../widget/struct.h"
#include "../widget/search.h"

#include "../private/get_widget_from_window.h"
#include "../private/process_configure_event.h"

#include "../events.h"
#include "../struct.h"

// update fields of widget:
	// old parent gets notifed it lost a child,
	// new parent gets notified it gained a child,
	// child gets notifed it changed parents

static void process_reparent_event(
	struct x11_loop* this,
	struct widget* child,
	XReparentEvent* event)
{
	struct widget *parent_was, *parent_is;
	ENTER;
	
	parent_was = child->parent;

	verpv(event->parent);
	
	if(event->parent == this->root_window)
	{
		parent_is = NULL;
	}
	else
	{
		parent_is = get_widget_from_window(this, event->parent);
	}
	
	verpv(parent_is);
	
	if(parent_was)
	{
		TODO; // remove child from list
		// if has an event handler:
		{
			TODO; // send event to parent, telling it that it lost a child
		}
	}
	
	child->parent = parent_is;
	
	child->x = event->x, child->y = event->y;
	
	if(parent_is)
	{
		// append child to list:
		{
			array_push_n(&(parent_is->children), &child);
		}
		
		// if has an event handler:
		if(parent_is->events->on_gained_child_notify)
		{
			// send event to parent, telling it that it gained a child:
			parent_is->events->on_gained_child_notify(
				parent_is,
				child,
				event);
		}
	}
	
	// if child has an event handler:
	if(child->events->on_reparented_notify)
	{
		TODO; // notify the child it has changed parents
	}
	
	
	EXIT;
}

void x11_loop_process_normal_events(struct x11_loop* this, XEvent* event)
{
	struct widget* widget;
	struct widget* child_widget;
	Display* display;
	ENTER;
	
	display = this->display;

	widget = get_widget_from_window(this, event->xany.window);
	
	if(widget)
	{
		switch(event->type)
		{
			case Expose:
			{
				verprintf("Normal: Expose\n");
				verpv(widget->events->on_expose);
				assert(widget->events->on_expose);
				widget->events->on_expose(widget, &(event->xexpose));
				break;
			}
			
			case MotionNotify:
			{
				verprintf("Normal: MotionNotify\n");
				verpv(widget->events->on_mouse_motion);
				if(widget->events->on_mouse_motion)
				{
					widget->events->on_mouse_motion(widget, &(event->xmotion));
				}
				break;
			}
			
			case EnterNotify:
			{
				verprintf("Normal: EnterNotify\n");
				verpv(widget->events->on_mouse_enter);
				widget->events->on_mouse_enter(widget, &(event->xcrossing));
				break;
			}
			
			case LeaveNotify:
			{
				verprintf("Normal: LeaveNotify\n");
				verpv(widget->events->on_mouse_leave);
				widget->events->on_mouse_leave(widget, &(event->xcrossing));
				break;
			}
			
			case ButtonPress:
			{
				verprintf("Normal: ButtonPress\n");
				verpv(widget->events->on_button_pressed);
				if(widget->events->on_button_pressed)
				{
					widget->events->on_button_pressed(widget, &(event->xbutton));
				}
				break;
			}
			
			case ButtonRelease:
			{
				verprintf("Normal: ButtonRelease\n");
				verpv(widget->events->on_button_released);
				widget->events->on_button_released(widget, &(event->xbutton));
				break;
			}
			
			case ConfigureNotify:
			{
				verprintf("Normal: ConfigureNotify\n");
				XConfigureEvent* spef = &(event->xconfigure);
				if(spef->window == spef->event)
				{
					verpv(widget->events->on_configure_notify);
					if(widget->events->on_configure_notify)
					{
						widget->events->on_configure_notify(widget, spef);
					}
					
					// update fields of widget
					process_configure_event(this, widget, spef);
				}
				else
				{
					
					child_widget = get_widget_from_window(this,
						spef->window);
					
					if(widget->events->on_child_configure_notify)
					{
						widget->events->on_child_configure_notify(
							widget, child_widget, spef);
					}
					
					// update fields of widget
					process_configure_event(this, child_widget, spef);
					
				}
				break;
			}
			
			case CreateNotify:
			{
				verprintf("Normal: CreateNotify\n");
				// set fields of widget
				TODO;
				break;
			}
			
			case DestroyNotify:
			{
				verprintf("Normal: DestroyNotify\n");
				// set fields of widget
				TODO;
				break;
			}
			
			case MapNotify:
			{
				XMapEvent* spef = &(event->xmap);
				if(spef->event == spef->window)
				{
					verprintf("Normal: MapNotify\n");
					verpv(widget->events->on_map);
					if(widget->events->on_map)
					{
						widget->events->on_map(widget, spef);
					}
				}
				break;
			}
			
			case UnmapNotify:
			{
				verprintf("Normal: UnmapNotify\n");
				XMapEvent* spef = &(event->xmap);
				if(spef->event == spef->window)
				{
					if(widget->events->on_unmap)
					{
						widget->events->on_unmap(widget, spef);
					}
				}
				break;
			}
			
			case ReparentNotify:
			{
				verprintf("Normal: ReparentNotify\n");
				XReparentEvent* spef = &(event->xreparent);
				if(spef->event == spef->window)
				{
					process_reparent_event(this, widget, spef);
				}
				break;
			}
			
			case ClientMessage:
			{
				verprintf("Normal: ClientMessage\n");
				if(widget->events->on_client_message)
				{
					widget->events->on_client_message(widget, event);
				}
				
				break;
			}
			
			default:
			{
				verpv(event->type);
				TODO;
				break;
			}
		}
	}
	else
	{
		TODO; // why am I getting events for an unknown window?
	}
	
	EXIT;
}


