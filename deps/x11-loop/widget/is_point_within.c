
#include <stdbool.h>

#include <debug.h>

#include "struct.h"

bool widget_is_point_within(
	struct widget* this,
	int x, int y)
{
	bool ret;
	ENTER;
	
	ret =
		true &&
		(0 <= x && x <= 0 + this->width) &&
		(0 <= y && y <= 0 + this->height);
	
	EXIT;
	return ret;
}
