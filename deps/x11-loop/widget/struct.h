
#ifndef WIDGET_H
#define WIDGET_H

#include <array/struct.h>

struct widget
{
	struct widget* parent;
	int window;
	int x, y;
	int width, height;
	struct array children; // elements of type 'struct widget*'
	struct events* events;
};

#endif
