
#include <X11/Xlib.h>
#include <stdlib.h>

#include <debug.h>

#include <array/new.h>

#include "struct.h"

struct widget* new_widget(
	struct events* events,
	Window window,
	int x, int y,
	int w, int h,
	int size)
{
	struct widget* this;
	ENTER;
	
	verpv(window);
	verpv(events);
	verpv(size);
	
	this = malloc(size);
	
	this->parent = NULL;
	this->window = window;
	this->events = events;
	
	this->x = x, this->y = y;
	this->width = w, this->height = h;
	
	this->children = new_array(struct widget*);
	
	EXIT;
	return this;
}
