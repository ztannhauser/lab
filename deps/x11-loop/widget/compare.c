
#include <stdio.h>
#include <assert.h>

#include <debug.h>

#include "../widget/struct.h"

int private_x11_loop_compare_widgets(struct widget* a, struct widget* b)
{
	int ret;
	ENTER;
	ret = a->window - b->window;
	verpv(ret);
	EXIT;
	return ret;
}
