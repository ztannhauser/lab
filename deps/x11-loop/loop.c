
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <stdbool.h>

#include <debug.h>

#include "struct.h"
#include "widget/struct.h"
#include "events.h"

#include "grabbed-mode/process_events.h"
#include "normal-mode/process_events.h"

void x11_loop_loop(struct x11_loop* this)
{
	bool was_timeout_list_nonempty = false;
	bool is_timeout_list_nonempty;
	XEvent event;
	Display* display;
	ENTER;
	
	display = this->display;
	
	while(this->should_continue)
	{
		while(this->should_continue && XPending(display))
		{
			XNextEvent(display, &event);
			
			verpv(event.xany.window);
			
			switch(this->mode)
			{
				case m_normal:
				{
					x11_loop_process_normal_events(this, &event);
					break;
				}
				
				case m_grabbed:
				{
					x11_loop_process_grabbed_events(this, &event);
					break;
				}
				
				default: TODO;
			}
		}
		
		// consider what to do when idling:
		{
			is_timeout_list_nonempty = this->timeout_functions.n > 0;
			if(was_timeout_list_nonempty < is_timeout_list_nonempty)
			{
				// initalize counters:
				TODO;
				was_timeout_list_nonempty = is_timeout_list_nonempty;
			}
			
			if(is_timeout_list_nonempty)
			{
				// calculate 'dt',
				// call all timeout functions with 'dt'
				TODO;
			}
		}
		
/*		usleep(500 * 1000);*/
		usleep(17 * 1000);
	}
	
	TODO;
	
	EXIT;
}





