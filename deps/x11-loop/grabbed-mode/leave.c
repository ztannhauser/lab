

//  XUngrabPointer(this->display, CurrentTime);

// free grabbed fields

// mode = s_normal;


#include <debug.h>

#include "../struct.h"

void x11_loop_leave_grabbed_mode(struct x11_loop* this)
{
	ENTER;
	
	verpv(this->mode);
	
	assert(this->mode == m_grabbed);
	
	this->mode = m_normal;
	
	EXIT;
}
