
#include <X11/Xlib.h>

#include <debug.h>

#include <avl/search.h>

#include "../struct.h"
#include "../events.h"
#include "../widget/struct.h"
#include "../widget/search.h"
#include "../widget/is_point_within.h"
#include "../private/process_configure_event.h"
#include "../private/get_widget_from_window.h"

// I have to keep track of which window the pointer is in, 
// and send enter and leave and motion and button up and down
// events, 

static void possibly_generialize(struct x11_loop* this, XMotionEvent* event)
{
	struct widget* hw;
	int x, y;
	ENTER;
	
	x = event->x_root, y = event->y_root;
	
	hw = this->grabbed_mode.hovering_widget;
	
	while(hw != NULL)
	{
		verpv(x);
		verpv(y);
		verpv(this->grabbed_mode.offset.x);
		verpv(this->grabbed_mode.offset.y);
		// if the mouse is no longer in 'hovering_widget':
		if(!widget_is_point_within(
			hw,
			x - this->grabbed_mode.offset.x,
			y - this->grabbed_mode.offset.y))
		{
			// send mouse leave event:
			{
				if(hw->events->on_mouse_leave)
				{
					hw->events->on_mouse_leave(hw, event);
				}
			}
			
			// subtract offset:
			{
				this->grabbed_mode.offset.x -= hw->x;
				this->grabbed_mode.offset.y -= hw->y;
			}
			
			// hovering_widget = hovering_widget->parent:
			{
				hw = hw->parent;
			}
		}
		else
		{
			break;
		}
	}
	
	this->grabbed_mode.hovering_widget = hw;
	
	EXIT;
}

static void possibly_specialize(struct x11_loop* this, XMotionEvent* event)
{
	bool cont;
	size_t i, n;
	int x_offseted, y_offseted;
	struct array* children;
	struct widget* ele;
	bool found_specialized_widget;
	int x, y;
	ENTER;
	
	x = event->x_root, y = event->y_root;
	
	for(cont = true;cont;)
	{
		// get list of children:
		{
			if(this->grabbed_mode.hovering_widget == NULL)
			{
				children = &(this->grabbed_mode.grabbed_widgets);
			}
			else
			{
				children = &(this->grabbed_mode.hovering_widget->children);
			}
		}
		
		verpv(children->n);
		
		// go through list, finding first child that contains (x, y):
		{
			found_specialized_widget = false;
			x_offseted = x - this->grabbed_mode.offset.x;
			y_offseted = y - this->grabbed_mode.offset.y;
			
			for(i = 0, n = children->n;!found_specialized_widget && i < n;i++)
			{
				ele = array_index(*children, i, struct widget*);
				
				verpv(ele->x);
				verpv(ele->y);
				
				if(widget_is_point_within(ele,
					x_offseted - ele->x,
					y_offseted - ele->y))
				{
					// send mouse enter event:
					{
						if(ele->events->on_mouse_enter)
						{
							ele->events->on_mouse_enter(ele, event);
						}
					}
					
					// adjust offset:
					{
						this->grabbed_mode.offset.x += ele->x;
						this->grabbed_mode.offset.y += ele->y;
					}
					
					// hovering_widget = ele:
					{
						this->grabbed_mode.hovering_widget = ele;
					}
					
					found_specialized_widget = true;
					
				}
			}
		}
		
		// if not found:
		{
			if(!found_specialized_widget)
			{
				cont = false;
			}
		}
	}
	EXIT;
}

void x11_loop_process_grabbed_events(struct x11_loop* this, XEvent* event)
{
	struct widget* widget;
	Display* display;
	ENTER;
	
	display = this->display;

	widget = get_widget_from_window(this, event->xany.window);
	
	if(widget)
	{
		switch(event->type)
		{
			case Expose:
			{
				verprintf("Grabbed: Expose\n");
				verpv(widget->events->on_expose);
				widget->events->on_expose(widget, &(event->xexpose));
				break;
			}
			
			case MotionNotify:
			{
				verprintf("Grabbed: MotionNotify\n");
				XMotionEvent* spef = &(event->xmotion);
				
				possibly_generialize(this, spef);
				
				possibly_specialize(this, spef);
				
				verpv(this->grabbed_mode.hovering_widget);
				
				if(this->grabbed_mode.hovering_widget == NULL)
				{
					if(this->grabbed_mode.root_events->on_mouse_motion)
					{
						this->grabbed_mode.root_events->on_mouse_motion(
							this->grabbed_mode.root_widget,
							spef);
					}
				}
				else
				{
					if(this->grabbed_mode.
						hovering_widget->events->on_mouse_motion)
					{
						this->grabbed_mode.
							hovering_widget->events->on_mouse_motion(
							this->grabbed_mode.hovering_widget,
							spef);
					}
				}
				
				break;
			}
			
			case ButtonPress:
			{
				verprintf("Grabbed: ButtonPress\n");
				XButtonEvent* spef = &(event->xbutton);
				if(this->grabbed_mode.hovering_widget == NULL)
				{
					if(this->grabbed_mode.root_events->on_button_pressed)
					{
						this->grabbed_mode.root_events->on_button_pressed(
							this->grabbed_mode.root_widget,
							spef);
					}
				}
				else
				{
					if(this->grabbed_mode.
						hovering_widget->events->on_button_pressed)
					{
						this->grabbed_mode.
							hovering_widget->events->on_button_pressed(
							this->grabbed_mode.hovering_widget,
							spef);
					}
				}
				
				break;
			}
			
			case ButtonRelease:
			{
				verprintf("Grabbed: ButtonRelease\n");
				XButtonEvent* spef = &(event->xbutton);
				
				if(this->grabbed_mode.hovering_widget == NULL)
				{
					if(this->grabbed_mode.root_events->on_button_released)
					{
						this->grabbed_mode.root_events->on_button_released(
							this->grabbed_mode.root_widget,
							spef);
					}
				}
				else
				{
					if(this->grabbed_mode.
						hovering_widget->events->on_button_released)
					{
						this->grabbed_mode.
							hovering_widget->events->on_button_released(
							this->grabbed_mode.hovering_widget,
							spef);
					}
				}
				
				break;
			}
			
			case ConfigureNotify:
			{
				verprintf("Grabbed: ConfigureNotify\n");
				XConfigureEvent* spef = &(event->xconfigure);
				if(spef->event == spef->window)
				{
					if(widget->x != spef->x || widget->y != spef->y)
					{
						verpv(widget->x);
						verpv(widget->y);
						verpv(spef->x);
						verpv(spef->y);
						
						// might have to adjust offset?
						{
							this->grabbed_mode.hovering_widget = NULL;
							this->grabbed_mode.offset.x = 0;
							this->grabbed_mode.offset.y = 0;
						}
					}
					
					verpv(widget->events->on_configure_notify);
					if(widget->events->on_configure_notify)
					{
						widget->events->on_configure_notify(widget, spef);
					}
					
					process_configure_event(this, widget, spef);
				}
				else
				{
					TODO; // call parent's 'on_child_configure_notify'
				}
				break;
			}
			
			case EnterNotify:
			{
				// why does this happen?
				verprintf("Grabbed: EnterNotify\n");
				verpv(widget->events->on_mouse_enter);
				widget->events->on_mouse_enter(widget, &(event->xcrossing));
				break;
			}
			
			case LeaveNotify:
			{
				verprintf("Grabbed: LeaveNotify\n");
				verpv(widget->events->on_mouse_leave);
				widget->events->on_mouse_leave(widget, &(event->xcrossing));
				break;
			}
			
			case MapNotify:
			{
				verprintf("Grabbed: MapNotify\n");
				XMapEvent* spef = &(event->xmap);
				if(spef->event == spef->window)
				{
					verpv(widget->events->on_map);
					if(widget->events->on_map)
					{
						widget->events->on_map(widget, spef);
					}
				}
				break;
			}
			
			case UnmapNotify:
			{
				verprintf("Grabbed: UnmapNotify\n");
				if(widget->events->on_unmap)
				{
					widget->events->on_unmap(widget, event);
				}
				
				break;
			}
			
			case ClientMessage:
			{
				verprintf("Grabbed: ClientMessage\n");
				if(widget->events->on_client_message)
				{
					widget->events->on_client_message(widget, event);
				}
				
				break;
			};
			
			default:
			{
				verprintf("Grabbed: default\n");
				verpv(event->type);
				TODO;
				break;
			}
		}
	}
	else
	{
		TODO; // why did I get an event from someone I don't know?
	}
	
	EXIT;
}





