
#include <debug.h>

#include <array/push_n.h>

#include "../struct.h"

void* x11_loop_grabbed_mode_push(struct x11_loop* this, struct widget* w)
{
	void* ret;
	ENTER;
	
	ret = w;
	
	verpv(ret);
	
	array_push_n(&(this->grabbed_mode.grabbed_widgets), &w);
	
	EXIT;
	return ret;
}
