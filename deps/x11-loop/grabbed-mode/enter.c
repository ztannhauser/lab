
// I'm also going to be given an events struct, which I will call
// if none of the known windows should get the event

#include <X11/Xlib.h>

#include <debug.h>

#include <array/new.h>

#include "../struct.h"
#include "../widget/struct.h"

void x11_loop_enter_grabbed_mode(
	struct x11_loop* this,
	struct widget* widget,
	int event_mask,
	struct events* root_events)
{
	ENTER;
	
	verpv(widget->window);
	
	assert(this->mode != m_grabbed);
	
	int ret = XGrabPointer(
		this->display,
		widget->window,
		False,
		event_mask,
		GrabModeAsync,
		GrabModeAsync,
		None, None,
		CurrentTime);
	
	if(ret != GrabSuccess)
	{
		verpv(ret);
		HERE;
		verpv(BadCursor);
		verpv(BadValue);
		verpv(BadWindow);
		HERE;
		verpv(GrabSuccess);
		verpv(GrabNotViewable);
		HERE;
		verpv(AlreadyGrabbed);
		verpv(GrabFrozen);
		verpv(GrabInvalidTime);
		CHECK;
	}
	
	this->grabbed_mode.offset.x = 0;
	this->grabbed_mode.offset.y = 0;
	this->grabbed_mode.root_widget = widget;
	this->grabbed_mode.hovering_widget = NULL;
	this->grabbed_mode.root_events = root_events;
	this->grabbed_mode.grabbed_widgets = new_array(struct widget*);
	this->mode = m_grabbed;
	
	EXIT;
}






















