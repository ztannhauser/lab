

#include <debug.h>

#include <array/pop.h>

#include "../struct.h"

void x11_loop_grabbed_mode_pop(struct x11_loop* this, void* ptr)
{
	size_t i, n;
	ENTER;
	
	verpv(this->mode);
	
	for(i = 0, n = this->grabbed_mode.grabbed_widgets.n;i < n;i++)
	{
		void* ele = array_index(this->grabbed_mode.grabbed_widgets, i, void*);
		if(ptr == ele)
		{
			array_pop(&(this->grabbed_mode.grabbed_widgets), i);
			break;
		}
	}
	
	if(i == n)
	{
		NOPE;
	}

	EXIT;
}
