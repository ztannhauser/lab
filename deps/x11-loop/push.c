
#include <assert.h>
#include <stdio.h>

#include <avl/insert.h>

#include <debug.h>

#include "struct.h"

#include "widget/struct.h"

void* x11_loop_push(struct x11_loop* this, struct widget* w)
{
	struct avl_node* ret;
	ENTER;
	verpv(w);
	verpv(w->window);
	ret = avl_insert(&(this->widgets), w);
	verpv(ret);
	EXIT;
	return ret;
}
