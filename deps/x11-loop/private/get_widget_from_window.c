
#include <X11/Xlib.h>

#include <debug.h>

#include <avl/search.h>

#include "../struct.h"
#include "../widget/search.h"

struct widget* get_widget_from_window(
	struct x11_loop* this,
	Window window)
{
	struct widget* ret;
	struct avl_node* widget_node;
	ENTER;
	
	widget_node = avl_tree_search(
		&(this->widgets),
		private_x11_loop_search_widget,
		&(window));
	
	if(widget_node)
	{
		ret = widget_node->data;
	}
	else
	{
		ret = NULL;
	}
	EXIT;
	return ret;
}
