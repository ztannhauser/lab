
#include <X11/Xlib.h>

#include <debug.h>

#include "../widget/struct.h"

void process_configure_event(
	struct x11_loop* this,
	struct widget* widget,
	XConfigureEvent* event)
{
	ENTER;
	
	verpv(widget);
	
	widget->x = event->x;
	widget->y = event->y;
	widget->width = event->width;
	widget->height = event->height;
	
	verpv(widget->x);
	verpv(widget->y);
	verpv(widget->width);
	verpv(widget->height);
	
	EXIT;
}

