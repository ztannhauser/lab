

#include <X11/Xlib.h>

#include "widget/struct.h"

#define INIT_ALL_FIELDS_NULL \
	.on_map = NULL, \
	.on_unmap = NULL, \
	.on_expose = NULL, \
	.on_mouse_enter = NULL, \
	.on_mouse_leave = NULL, \
	.on_mouse_motion = NULL, \
	.on_button_pressed = NULL, \
	.on_button_released = NULL, \
	.on_configure_notify = NULL, \
	.on_client_message = NULL, \
	\
	.on_gained_child_notify = NULL, \
	.on_lost_child_notify = NULL, \
	.on_child_configure_notify = NULL \
	

struct events
{
	struct // events for components:
	{
		// 'widget' has been mapped:
		void (*on_map)(
			struct widget* widget,
			XMapEvent* event);
		
		// 'widget' has been unmapped:
		void (*on_unmap)(
			struct widget* widget,
			XMapEvent* event);
		
		// 'widget' has been exposed: (needs to redraw)
		void (*on_expose)(
			struct widget* widget,
			XExposeEvent* event);
		
		// the mouse has entered the pixel space of 'widget':
		void (*on_mouse_enter)(
			struct widget* widget,
			XEnterWindowEvent* event);
		
		// the mouse has left the pixel space of 'widget':
		void (*on_mouse_leave)(
			struct widget* widget,
			XLeaveWindowEvent* event);
		
		// the mouse has moved in the pixel space of 'widget':
		void (*on_mouse_motion)(
			struct widget* widget,
			XMotionEvent* event);
		
		// one of the mouse's buttons has been pressed inside of 'widget':
		void (*on_button_pressed)(
			struct widget* widget,
			XButtonEvent* event);
		
		// one of the mouse's buttons has been released inside of 'widget':
		void (*on_button_released)(
			struct widget* widget,
			XButtonEvent* event);
		
		// 'widget' has been (re)configured:
		void (*on_configure_notify)(
			struct widget* widget,
			XConfigureEvent* event);
		
		// 'widget' has been sent a message from a client:
		void (*on_client_message)(
			struct widget* widget,
			XClientMessageEvent* event);
		
	};
	
	struct // events for containers:
	{
		// 'widget' has been reparented:
		void (*on_reparented_notify)(
			struct widget* widget,
			struct widget* new_parent,
			XConfigureEvent* event);
		
		// 'widget' has gained a new child:
		void (*on_gained_child_notify)(
			struct widget* widget,
			struct widget* new_child,
			XConfigureEvent* event);
		
		// 'widget' has lost a child:
		void (*on_lost_child_notify)(
			struct widget* widget,
			struct widget* lost_child,
			XConfigureEvent* event);
		
		// one of 'widget's children has been (re)configued:
		void (*on_child_configure_notify)(
			struct widget* widget,
			struct widget* child,
			XConfigureEvent* event);
	};
};



















