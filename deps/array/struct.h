
#ifndef ARRAY_H
#define ARRAY_H

#include <inttypes.h>

struct array
{
	union {
		unsigned char *data;
		signed char *datas;
		char *datac;
		void *datavp;
		void **datavpp;
	};
	uintmax_t n;
	uintmax_t elesize;
	uintmax_t mem_length;
};

#define arrayptr_index(array, index)\
	array_index(array, index, void*)

#define array_index(array, index, datatype)\
	(((datatype *) ((array).data))[index])

#define arrayp_index(array, index)\
	((array).data + (index) * (array).elesize)

#endif
