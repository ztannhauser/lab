

struct array* array_init(struct array* this, int elesize);

struct array* array_init_given_data(
	struct array* this,
	const char* data, unsigned long n,
	int elesize);
