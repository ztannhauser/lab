
#include <stdio.h>
#include <X11/Xlib.h>

#include <debug.h>

#include <x11-loop/new.h>
#include <x11-loop/loop.h>
#include <x11-loop/delete.h>

#include <main/struct.h>
#include <main/new.h>
#include <main/delete.h>

int depth = 0;

int main()
{
	struct x11_loop* loop;
	struct main_window* win;
	Display* display;
	ENTER;
	
	display = XOpenDisplay(NULL);
	
	loop = new_x11_loop(display);
	
	win = new_main_window(display, loop);

	XMapWindow(display, win->super.window);
	
	x11_loop_loop(loop);
	
	delete_main_window(win);
	
	delete_x11_loop(loop);
	
	XCloseDisplay(display);
	
	EXIT;
	return 0;
}






















