
#include <stdio.h>
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>

#include <debug.h>

#include "on_expose.h"

#define GRID_ORIGIN_COLOR (0x000000)
#define GRID_LINES_COLOR (0xCCCCCC)
#define GRID_SPACING (200)

void main_window_on_expose(struct main_window* this, XExposeEvent* event)
{
	int low;
	int high;
	int width, height;
	Display* display;
	Drawable draw;
	GC gc;
	int cx, cy;
	ENTER;
	
	display = this->display;
	draw = this->backbuffer;
	gc = this->gc;
	cx = this->camera_pos.x, cy = this->camera_pos.y;
	width = this->super.width;
	height = this->super.height;
	
	// draw grid lines:
	{
		XSetForeground(display, this->gc, GRID_LINES_COLOR);
		
		// vertical lines:
		{
			low = this->camera_pos.x / GRID_SPACING * GRID_SPACING;
			high = (this->camera_pos.x + width)
				/ GRID_SPACING * GRID_SPACING;
			for(int x = low; x <= high;x += GRID_SPACING)
			{
				XDrawLine(display, draw, gc, x - cx, 0, x - cx, height);
			}
		}
		
		// horizontal lines:
		{
			low = this->camera_pos.y / GRID_SPACING * GRID_SPACING;
			high = (this->camera_pos.y + height)
				/ GRID_SPACING * GRID_SPACING;
			for(int y = low; y <= high;y += GRID_SPACING)
			{
				XDrawLine(display, draw, gc, 0, y - cy, width, y - cy);
			}
		}
		
		// axis lines:
		{
			XSetForeground(display, this->gc, GRID_ORIGIN_COLOR);
			XDrawLine(display, draw, gc, 0 - cx, 0, 0 - cx, height);
			XDrawLine(display, draw, gc, 0, 0 - cy, width, 0 - cy);
		}
	}











	// very last line: (make backbuffer visible)
	{
		XdbeSwapBuffers(display, &(this->swapinfo), 1);
	}
	
	EXIT;
}




























