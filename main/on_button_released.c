
#include <X11/Xlib.h>

#include <debug.h>

#include <x11-menu/container/map.h>

#include "struct.h"

void main_window_on_button_released(
	struct main_window* this, XButtonEvent* event)
{
	ENTER;
	switch(this->state)
	{
		case s_normal:
		{
			switch(event->button)
			{
				case Button1: break;
				case Button3: 
				{
					
					menu_container_map(this->pallette,
						event->x_root, event->y_root,
						this->super.window,
						this->menu_atom);
					
					break;
				}
			}
			break;
		}
		case s_moving:
		{
			this->state = s_normal;
			break;
		}
		default: TODO;
	}
	EXIT;
}
