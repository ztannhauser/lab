
#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <X11/Xlib.h>
#include <X11/extensions/Xdbe.h>

#include <x11-loop/widget/struct.h>

enum state
{
	s_normal,
	s_moving,
	s_n
};

struct main_window
{
	struct widget super; // must be first
	Display* display;
	struct x11_loop* loop;
	void* loop_token;
	
	enum state state;
	
	struct {int x, y;} camera_pos;
	
	struct menu_container* pallette;
	Atom menu_atom;
	
	struct // commonly used x11 stuff:
	{
		int screen;
		GC gc;
	};
	
	struct // backbuffer stuff
	{
		XdbeBackBuffer backbuffer;
		XdbeSwapInfo swapinfo;
	};
	
	struct // normal state fields:
	{
		
	} normal;
	
	struct // moving camera state fields:
	{
		struct {int x, y;} last_mouse_pos;
	} moving ;
	
};

#endif 





















