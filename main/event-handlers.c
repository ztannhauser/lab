
#include <x11-loop/events.h>

#include "on_expose.h"
#include "on_button_pressed.h"
#include "on_button_released.h"
#include "on_mouse_motion.h"
#include "on_client_message.h"
#include "on_configure_notify.h"

struct events main_window_event_handlers = 
{
	INIT_ALL_FIELDS_NULL,
	.on_expose = main_window_on_expose,
	.on_button_pressed = main_window_on_button_pressed,
	.on_button_released = main_window_on_button_released,
	.on_mouse_motion = main_window_on_mouse_motion,
	.on_configure_notify = main_window_on_configure_notify,
	.on_client_message = main_window_on_client_message,
};
