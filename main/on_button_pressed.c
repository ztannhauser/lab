
#include <X11/Xlib.h>

#include <debug.h>

#include "struct.h"

void main_window_on_button_pressed(struct main_window* this, XButtonEvent* event)
{
	ENTER;
	
	switch(this->state)
	{
		case s_normal:
		{
			switch(event->button)
			{
				case Button1:
				{
					this->moving.last_mouse_pos.x = event->x;
					this->moving.last_mouse_pos.y = event->y;
					this->state = s_moving;
					break;
				}
				case Button3: break;
			}
			break;
		}
		default: TODO;
	}
	
	
	EXIT;
}
