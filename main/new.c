
#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <debug.h>

#include <x11-loop/widget/new.h>
#include <x11-loop/push.h>

#include <x11-menu/container/new.h>
#include <x11-menu/row/entry/new.h>
#include <x11-menu/row/submenu/new.h>

#include "struct.h"
#include "event-handlers.h"

#define DEFAULT_COLOR (-1)

struct main_window* new_main_window(Display* display, struct x11_loop* loop)
{
	struct main_window* this;
	Window window;
	ENTER;
	
	// inital X11 stuff:
	{
		// setup widget:
		{
			this = new_widget(
				&main_window_event_handlers,
				window = XCreateSimpleWindow(display,
					DefaultRootWindow(display),
					0, 0,
					400,
					400,
					0, 0,
					DEFAULT_COLOR
				),
				0, 0,
				400, 400,
				sizeof(struct main_window)
			);
		}
		
		// register and set event mask:
		{
			this->loop_token = x11_loop_push(loop, this);
			XSelectInput(display, window, EVENT_MASK);
		}
		
		// prepare commonly used x11 values:
		{
			this->loop = loop;
			this->display = display;
			this->screen = DefaultScreen(display);
		}
	}

	// X11 set window gravity (to stop the terrible white-flashing)
	{
		XSetWindowAttributes winattrib = {
			.bit_gravity = NorthWestGravity  // CenterGravity
		};
		XChangeWindowAttributes(display, window, CWBitGravity, &winattrib);
	}
	
	// X11 backbuffer stuff:
	{
		this->swapinfo.swap_window = window;
		this->swapinfo.swap_action = XdbeBackground;
		this->backbuffer =
			XdbeAllocateBackBufferName(display, window, XdbeBackground);
	}
	
	// create GC:
	{
		this->gc = XCreateGC(display, window, 0, NULL);
	}
	
	// init menu_atom:
	{
		this->menu_atom = XInternAtom(display, "Menu Selection", False);
	}
	
	// create pallette:
	{
		this->pallette = new_menu_container(display, loop,
			new_menu_entry(display, loop, "Option #1", "Data #1"),
			new_menu_entry(display, loop, "Option #2", "Data #2"),
			new_menu_entry(display, loop, "Option #3", "Data #3"),
			new_submenu_entry(display, loop, "Option #4 ->",
				new_menu_container(display, loop,
					new_menu_entry(display, loop, "Option #4.1", "Data #4.1"),
					new_menu_entry(display, loop, "Option #4.2", "Data #4.2"),
					new_menu_entry(display, loop, "Option #4.3", "Data #4.3"),
					NULL
				)
			),
			new_submenu_entry(display, loop, "Option #5 ->",
				new_menu_container(display, loop,
					new_menu_entry(display, loop, "Option #5.1", "Data #5.1"),
					new_submenu_entry(display, loop, "Option #5.2 ->",
						new_menu_container(display, loop,
							new_menu_entry(display, loop, "Option #5.2.1", "Data #5.2.1"),
							new_menu_entry(display, loop, "Option #5.2.2", "Data #5.2.2"),
							new_menu_entry(display, loop, "Option #5.2.3", "Data #5.2.3"),
							NULL
						)
					),
					new_menu_entry(display, loop, "Option #5.3", "Data #5.3"),
					NULL
				)
			),
			new_menu_entry(display, loop, "Option #6", "Data #6"),
			new_menu_entry(display, loop, "Option #7", "Data #7"),
			NULL
		);
	}

	this->camera_pos.x = 0, this->camera_pos.y = 0;
	
	this->state = s_normal;



	
	EXIT;
	return this;
}






















