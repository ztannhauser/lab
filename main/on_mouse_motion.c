
#include <X11/Xlib.h>

#include <debug.h>

#include "struct.h"
#include "on_expose.h"

void main_window_on_mouse_motion(
	struct main_window* this,
	XMotionEvent* event)
{
	ENTER;
	switch(this->state)
	{
		case s_normal: break;
		case s_moving:
		{
			this->camera_pos.x -= (event->x - this->moving.last_mouse_pos.x);
			this->camera_pos.y -= (event->y - this->moving.last_mouse_pos.y);
			
			main_window_on_expose(this, NULL);

			this->moving.last_mouse_pos.x = event->x;
			this->moving.last_mouse_pos.y = event->y;
			break;
		}
		default: TODO;
	}
	EXIT;
}
