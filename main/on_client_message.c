
#include <X11/Xlib.h>
#include <string.h>

#include <debug.h>

#include "struct.h"

void main_window_on_client_message(
	struct main_window* this,
	XClientMessageEvent* event)
{
	ENTER;
	
	if(event->message_type == this->menu_atom)
	{
		verpv(event->format);
		void* menu_entry_value;
		memcpy(&menu_entry_value, event->data.l, sizeof(void*));
		verpvs(menu_entry_value);
		XStoreName(this->display, this->super.window, menu_entry_value);
	}
	
	EXIT;
}
