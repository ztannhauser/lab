

#define EVENT_MASK (\
	ExposureMask | \
	PointerMotionMask | \
	ButtonPressMask | \
	ButtonReleaseMask | \
	StructureNotifyMask | \
	SubstructureNotifyMask | \
	0 )

extern struct events main_window_event_handlers;
