

CC = gcc
CPPFLAGS = -I . -I deps
CFLAGS += -Wall
CFLAGS += -Werror=implicit-function-declaration
CFLAGS += -Werror=discarded-qualifiers

DFLAGS += -g
DFLAGS += -D DEBUGGING=1

LDLIBS = -lX11 -lXext

# ARGS = 

TARGET = lab

default: $(TARGET)

install: ~/bin/$(TARGET)

~/bin/$(TARGET): $(TARGET)
	cp $(TARGET) ~/bin/

run: $(TARGET)
	./$(TARGET) $(ARGS)

# --gen-suppressions=yes 

valrun: $(TARGET).d
	valgrind ./$(TARGET).d $(ARGS)

srclist.mk:
	find -name '*.c' ! -path './#*' | sed 's/^/SRCS += /' > srclist.mk

include srclist.mk

OBJS = $(SRCS:.c=.o)
DOBJS = $(SRCS:.c=.d.o)
DEPENDS = $(SRCS:.c=.mk)

$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) $(LOADLIBES) $(LDLIBS) -o $@

$(TARGET).d: $(DOBJS)
	$(CC) $(LDFLAGS) $(DOBJS) $(LOADLIBES) $(LDLIBS) -o $@

%.l.mk: %.l
	echo "$(basename $<).c: $<" > $@

%.y.mk: %.y
	echo "$(basename $<).c: $<" > $@

%.mk: %.c
	$(CPP) -MM -MT $@ $(CPPFLAGS) -MF $@ $< || (gedit $< && false)

%.o: %.c %.mk
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@ || (gedit $< && false)

%.d.o: %.c %.mk
	$(CC) -c $(DFLAGS) $(CPPFLAGS) $(CFLAGS) $< -o $@ || (gedit $< && false)

.PHONY: clean deep-clean

clean:
	rm -f $(OBJS) $(DEPENDS)
	rm -f srclist.mk
	rm -f $(TARGET)

deep-clean:
	find -type f -regex '.*\.mk' -delete
	find -type f -regex '.*\.o' -delete
	find -type f -executable -delete

include $(DEPENDS)

